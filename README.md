# deploy-wp
## Descrição

deploy-wp é um código de automação de wordpress, através dele é possível criar um novo wordpress do zero em máquinas virtuais específicas

## Instalação Local
Para rodar localmente o ambiente de staging por exemplo, basta ter o ansible instalado na máquina local sem nenhum plugin adicional.
Exemplo para o webstite com domínio teste-brius.com.br

```
ansible-playbook -i hosts playbook-staging.yml --extra-vars "domain=teste-brius.com.br database_schema=teste_brius_com_br database_instance_dns_name=mysql-1.staging.etus.local database_user=user1234 database_pass=secret1234"
```

A execução local não cria a base de dados automaticamente, necessitando executar manualmente com as devidas credenciais de SQL Editor no mínimo.

```
gcloud sql databases create teste_brius_com_br -i mysql-stag-1 --project etus-media-development-staging
```

## Como Usar
O uso desse código deve ser feito pelo próprio gitlab executando uma pipeline manualmente com variáveis específicas, a falta da variável implica em um ou mais jobs não serem executados.

1.  Executar a pipeline do repositório manualmene -> https://gitlab.com/brius/sre/devops/deploy-wp/-/pipelines
    1. Preencher as seguines variáveis:
  1. SITE_DOMAIN
        2. DATABASE_SCHEMA
        3. DATABASE_INSTANCE
        4. DATABASE_INSTANCE_DNS_NAME

Exemplo

1. SITE_DOMAIN = teste-brius.com.br
2. DATABASE_SCHEMA = teste_brius_com_br
3. DATABASE_INSTANCE = mysql-stag-1
4. DATABASE_INSTANCE_DNS_NAME = mysql-1.staging.etus.local

### Tarefas externas
Atualmente o código cria as dependências para wordpress na VM, ainda é necessário intervenção manual na criação do apontamento de DNS para a VM via Cloudflare

## Projetos Futuros
1. Usar usuário e senha randômico por website
2. Criar a entrada de DNS automaticamente

## Autores(as)
[@Naynivek](https://gitlab.com/naynivek)
